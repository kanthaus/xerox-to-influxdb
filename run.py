#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
from xml.etree import ElementTree
from influxdb import InfluxDBClient
import time

from config import influxdb_config

response = requests.post(
    "http://xc6caadb.local/ssm/Management/Anonymous/StatusConfig",
    data="""<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><msg:MessageInformation xmlns:msg="http://www.fujixerox.co.jp/2014/08/ssm/management/message"><msg:MessageExchangeType>RequestResponse</msg:MessageExchangeType><msg:MessageType>Request</msg:MessageType><msg:Action>http://www.fujixerox.co.jp/2003/12/ssm/management/statusConfig#GetAttribute</msg:Action><msg:From><msg:Address>http://www.fujixerox.co.jp/2014/08/ssm/management/soap/epr/client</msg:Address><msg:ReferenceParameters/></msg:From></msg:MessageInformation></soap:Header><soap:Body><cfg:GetAttribute xmlns:cfg="http://www.fujixerox.co.jp/2003/12/ssm/management/statusConfig"><cfg:Object name="urn:fujixerox:names:ssm:1.0:management:PrintUsageCounter" offset="0"/><cfg:Object name="urn:fujixerox:names:ssm:1.0:management:CopyUsageCounter" offset="0"/><cfg:Object name="urn:fujixerox:names:ssm:1.0:management:LargeSizeUsageCounter" offset="0"/><cfg:Object name="urn:fujixerox:names:ssm:1.0:management:MarkerSupply" offset="0"/></cfg:GetAttribute></soap:Body></soap:Envelope>""",
    headers={
        "soapAction": "http://www.fujixerox.co.jp/2003/12/ssm/management/statusConfig#GetAttribute",
        "Content-Type": "text/xml",
    },
)

tree = ElementTree.fromstring(response.content)

fields = {}

for o in tree.iter(
    "{http://www.fujixerox.co.jp/2003/12/ssm/management/statusConfig}Object"
):
    if o.attrib["name"].endswith("PrintUsageCounter"):
        for counter in o:
            fields[counter.attrib["name"]] = int(counter.text)
    if o.attrib["name"].endswith("MarkerSupply"):
        level = o.find(".//*[@name='Remaining']")
        pages = o.find(".//*[@name='PageRemaining']")
        name = o.find(".//*[@name='Name']")
        if name is None or level is None:
            continue
        fields[name.text + "_Percentage"] = int(level.text)
        fields[name.text + "_PageRemaining"] = int(pages.text)

points = [
    {
        "measurement": "printer",
        "tags": {"type": "xerox phaser 6510",},
        "time": round(time.time()),
        "fields": fields,
    }
]


client = InfluxDBClient(**influxdb_config)
client.write_points(points, time_precision="s")
